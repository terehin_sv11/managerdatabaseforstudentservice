#include "subjectmodel.h"

SubjectModel::SubjectModel()
{
    this->id = "-1";
    this->name = "";
    this->nameTask = "";
    this->descriptionTask = "";
    this->literatureTask = "";
}

SubjectModel::SubjectModel(QString id,QString name,QString nameTask,QString descriptionTask,QString literatureTask)
{
    this->id = id;
    this->name = name;
    this->nameTask = nameTask;
    this->descriptionTask = descriptionTask;
    this->literatureTask = literatureTask;
}
