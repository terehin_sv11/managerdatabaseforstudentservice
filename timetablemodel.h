#ifndef TIMETABLEMODEL_H
#define TIMETABLEMODEL_H
#include <QString>

class TimetableModel
{


public:
    TimetableModel();
    TimetableModel(QString id, QString nameSubject, QString time, QString nameTeacher,QString group,QString classRoom);
    QString id;
    QString nameSubject;
    QString time;
    QString nameTeacher;
    QString group;
    QString classRoom;
private:

};

#endif // TIMETABLEMODEL_H
