#ifndef SQLREQUESTHELPER_H
#define SQLREQUESTHELPER_H
#include <QSql>
#include <QSqlDatabase>
#include <QDebug>
#include "usermodel.h"
#include <QSqlQueryModel>
#include <QSqlRecord>
#include <QSqlQuery>
#include "timetablemodel.h"
#include "literaturemodel.h"
#include "subjectmodel.h"

class SqlRequestHelper: public QObject
{
public:

    char dm = 39;
    static SqlRequestHelper* getInstance();

    QSqlDatabase sqlDatabase;

    QVector<UserModel*> userModels;
    QVector<TimetableModel*> timetableModels;
    QVector<LiteratureModel*> literatureModels;
    QVector<SubjectModel*> subjectModels;

    bool connectionToDB(QString login, QString password);

    QVector<UserModel*> getUserModels();
    QVector<TimetableModel*> getTimetableModels();
    QVector<LiteratureModel*> getLiteratureModels();
    QVector<SubjectModel*> getSubjectModels();


    bool insertNewUser(UserModel *userModel);
    bool insertNewSubjectInTimetable(TimetableModel * timetableModel);
    bool insertNewBook(LiteratureModel *literatureModel);
    bool insertNewSubjectTask(SubjectModel *subjectModel);

    int qetLastId();

    bool updateUser(UserModel userModel);
    bool updateTimetable(TimetableModel *timetableModel, int index);
    bool updateBook(LiteratureModel *literatureModel, int index);
    bool updateTask(SubjectModel *subjectModel, int index);

    bool deleteUser(QString id);
    bool deleteTimetableSubject(QString id);
    bool deleteBook(QString id);
    bool deleteTask(QString id);

private:
    SqlRequestHelper(){ }
    SqlRequestHelper(SqlRequestHelper const&){}
    SqlRequestHelper& operator=(SqlRequestHelper const&){}


};

#endif // SQLREQUESTHELPER_H
