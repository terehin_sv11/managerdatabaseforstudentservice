#include "timetablemodel.h"

TimetableModel::TimetableModel()
{
    this->id = -1;
    this->classRoom = "";
    this->nameSubject = "";
    this->time = "";
    this->nameTeacher = "";
    this->group = "";
}

TimetableModel::TimetableModel(QString id, QString nameSubject, QString time, QString nameTeacher,QString group,QString classRoom)
{
    this->id = id;
    this->classRoom = classRoom;
    this->nameSubject = nameSubject;
    this->time = time;
    this->nameTeacher = nameTeacher;
    this->group = group;
}
