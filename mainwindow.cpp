#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QTableView>
#include <QTableWidget>
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->user_tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->user_tabWidget->setTabText(0,"User");
    ui->user_tabWidget->setTabText(1,"Timetable");

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_connectionPushButton_clicked()
{
    QString login = ui->login_lineEdit->text();
    QString password = ui->passwordCon_lineEdit->text();
    connect = reconectToDB(login,password);
    if (connect) {
        refreshUserTable();
        refreshTimetableTable();
        refreshLiteratureTable();
        refreshTaskTable();
    }
}

bool MainWindow::reconectToDB(QString login, QString password) {
    if (sqlHepler->connectionToDB(login,password)) {
        connect = true;
        return true;
    }
    else {
        QMessageBox::warning(0,"Connection failed", "Connection does not established");
        connect = false;
        return false;
    }
}


void MainWindow::on_update_pushButton_clicked()
{
    UserModel userModel = *new UserModel();
    userModel.firstName = ui->firstName_lineEdit->text();
    userModel.lastName = ui->lastName_lineEdit->text();
    userModel.email = ui->email_lineEdit->text();
    userModel.course = ui->course_lineEdit->text();
    userModel.password = ui->password_lineEdit->text();
    int id = selectedUserId;
    QString id_str = QString::number(id);
    userModel.id = id_str;
    if (userModel.firstName != "" && userModel.lastName != "" && userModel.email != "" && userModel.course != "" && userModel.password != "" && selectedUserId != -1 ) {
        sqlHepler->updateUser(userModel);
        refreshUserTable();
    }
}

void MainWindow::on_user_tableWidget_clicked(const QModelIndex &index)
{
    clearUserSelected();

    int modelIndex = ui->user_tableWidget->selectionModel()->currentIndex().row();
    selectedUserId = sqlHepler->userModels[modelIndex]->id.toInt();

    ui->firstName_lineEdit->setText(sqlHepler->userModels[modelIndex]->firstName);
    ui->lastName_lineEdit->setText(sqlHepler->userModels[modelIndex]->lastName);
    ui->email_lineEdit->setText(sqlHepler->userModels[modelIndex]->email);
    ui->course_lineEdit->setText(sqlHepler->userModels[modelIndex]->course);
    ui->password_lineEdit->setText(sqlHepler->userModels[modelIndex]->password);
}

void MainWindow::clearUserSelected()
{
    ui->firstName_lineEdit->clear();
    ui->lastName_lineEdit->clear();
    ui->email_lineEdit->clear();
    ui->course_lineEdit->clear();
    ui->password_lineEdit->clear();
}

void MainWindow::clearTimetableSelected()
{
    ui->group_lineEdit->clear();
    ui->nameSub_lineEdit->clear();
    ui->nameTeacher_lineEdit->clear();
    ui->time_lineEdit->clear();
    ui->classroom_lineEdit->clear();
}

void MainWindow::clearBookSelected()
{
    ui->subject_lineEdit->clear();
    ui->courseLit_lineEdit->clear();
    ui->nameBook_lineEdit->clear();
    ui->author_lineEdit->clear();
    ui->url_lineEdit->clear();
}

void MainWindow::clearTaskSelected()
{
    ui->nameSubjectForTask_lineEdit->clear();
    ui->nameTask_lineEdit->clear();
    ui->descriptionTask_lineEdit->clear();
    ui->literatureTask_lineEdit->clear();
}

void MainWindow::on_clear_pushButton_clicked()
{
    selectedUserId = -1;
    clearUserSelected();
}

void MainWindow::on_add_pushButton_clicked()
{
    UserModel *userModel = new UserModel();
    int id = sqlHepler->qetLastId() + 1;
    QString s = QString::number(id);
    userModel->id = s;
    userModel->firstName = ui->firstName_lineEdit->text();
    userModel->lastName = ui->lastName_lineEdit->text();
    userModel->email = ui->email_lineEdit->text();
    userModel->course = ui->course_lineEdit->text();
    userModel->password = ui->password_lineEdit->text();
    sqlHepler->userModels.append(userModel);

    if (userModel->firstName != "" && userModel->lastName != "" && userModel->email != "" && userModel->course != "" && userModel->password != "" ) {
        sqlHepler->insertNewUser(userModel);
        refreshUserTable();
    }
    else {
        QMessageBox::information(0,"Error Insert","Fill in all fields before adding");
    }
}

void MainWindow::refreshUserTable() {
    ui->user_tableWidget->clear();
    QVector<UserModel*> userModels = sqlHepler->getUserModels();
    ui->user_tableWidget->setRowCount(userModels.size()); // указываем количество строк
    for (int j = 0; j < ui->user_tableWidget->colorCount(); j++)
        for(int i = 0; i<=ui->user_tableWidget->rowCount(); i++)
            ui->user_tableWidget->setItem(i, j, new QTableWidgetItem());

    for(int i = 0; i < ui->user_tableWidget->rowCount(); i++)
        for(int j = 0; j < ui->user_tableWidget->columnCount(); j++)
        {
            QTableWidgetItem *item = new QTableWidgetItem(); // выделяем память под ячейку
            switch (j) {
            case 0:
                item->setText(QString(userModels[i]->id));
                break;
            case 1:
                item->setText(QString(userModels[i]->firstName));
                break;
            case 2:
                item->setText(QString(userModels[i]->lastName));
                break;
            case 3:
                item->setText(QString(userModels[i]->email));
                break;
            case 4:
                item->setText(QString(userModels[i]->course));
                break;
            case 5:
                item->setText(QString(userModels[i]->password));
                break;
            }

            ui->user_tableWidget->setItem(i, j, item); // вставляем ячейку
        }

    ui->user_tableWidget->setHorizontalHeaderItem(0, new QTableWidgetItem("Id"));
    ui->user_tableWidget->setHorizontalHeaderItem(1, new QTableWidgetItem("First name"));
    ui->user_tableWidget->setHorizontalHeaderItem(2, new QTableWidgetItem("Last name"));
    ui->user_tableWidget->setHorizontalHeaderItem(3, new QTableWidgetItem("Email"));
    ui->user_tableWidget->setHorizontalHeaderItem(4, new QTableWidgetItem("Course"));
    ui->user_tableWidget->setHorizontalHeaderItem(5, new QTableWidgetItem("Password"));

}

void MainWindow::refreshTimetableTable()
{
    ui->timatable_tableWidget->clear();
    QVector<TimetableModel*> timetableModels = sqlHepler->getTimetableModels();
    ui->timatable_tableWidget->setRowCount(timetableModels.size());
    for (int j = 0; j < ui->timatable_tableWidget->colorCount(); j++)
        for(int i = 0; i< ui->timatable_tableWidget->rowCount(); i++)
            ui->timatable_tableWidget->setItem(i, j, new QTableWidgetItem());

    for(int i = 0; i < ui->timatable_tableWidget->rowCount(); i++)
        for(int j = 0; j < ui->timatable_tableWidget->columnCount(); j++)
        {
            QTableWidgetItem *item = new QTableWidgetItem(); // выделяем память под ячейку
            switch (j) {
            case 0:
                item->setText(QString(timetableModels[i]->id));
                break;
            case 1:
                item->setText(QString(timetableModels[i]->nameSubject));
                break;
            case 2:
                item->setText(QString(timetableModels[i]->time));
                break;
            case 3:
                item->setText(QString(timetableModels[i]->nameTeacher));
                break;
            case 4:
                item->setText(QString(timetableModels[i]->group));
                break;
            case 5:
                item->setText(QString(timetableModels[i]->classRoom));
                break;
            }

            ui->timatable_tableWidget->setItem(i, j, item); // вставляем ячейку
        }

    ui->timatable_tableWidget->setHorizontalHeaderItem(0, new QTableWidgetItem("Id"));
    ui->timatable_tableWidget->setHorizontalHeaderItem(1, new QTableWidgetItem("Name subject"));
    ui->timatable_tableWidget->setHorizontalHeaderItem(2, new QTableWidgetItem("Time"));
    ui->timatable_tableWidget->setHorizontalHeaderItem(3, new QTableWidgetItem("Name teacher"));
    ui->timatable_tableWidget->setHorizontalHeaderItem(4, new QTableWidgetItem("Group"));
    ui->timatable_tableWidget->setHorizontalHeaderItem(5, new QTableWidgetItem("Classroom"));
}

void MainWindow::refreshLiteratureTable()
{
    ui->literature_tableWidget->clear();
    QVector<LiteratureModel*> literatureModel = sqlHepler->getLiteratureModels();
    ui->literature_tableWidget->setRowCount(literatureModel.size());
    for (int j = 0; j < ui->literature_tableWidget->colorCount(); j++)
        for(int i = 0; i< ui->literature_tableWidget->rowCount(); i++)
            ui->literature_tableWidget->setItem(i, j, new QTableWidgetItem());

    for(int i = 0; i < ui->literature_tableWidget->rowCount(); i++)
        for(int j = 0; j < ui->literature_tableWidget->columnCount(); j++)
        {
            QTableWidgetItem *item = new QTableWidgetItem(); // выделяем память под ячейку
            switch (j) {
            case 0:
                item->setText(QString(literatureModel[i]->id));
                break;
            case 1:
                item->setText(QString(literatureModel[i]->subject));
                break;
            case 2:
                item->setText(QString(literatureModel[i]->course));
                break;
            case 3:
                item->setText(QString(literatureModel[i]->nameBook));
                break;
            case 4:
                item->setText(QString(literatureModel[i]->author));
                break;
            case 5:
                item->setText(QString(literatureModel[i]->url));
                break;
            }

            ui->literature_tableWidget->setItem(i, j, item); // вставляем ячейку
        }

    ui->literature_tableWidget->setHorizontalHeaderItem(0, new QTableWidgetItem("Id"));
    ui->literature_tableWidget->setHorizontalHeaderItem(1, new QTableWidgetItem("Subject"));
    ui->literature_tableWidget->setHorizontalHeaderItem(2, new QTableWidgetItem("Course"));
    ui->literature_tableWidget->setHorizontalHeaderItem(3, new QTableWidgetItem("Name book"));
    ui->literature_tableWidget->setHorizontalHeaderItem(4, new QTableWidgetItem("Author"));
    ui->literature_tableWidget->setHorizontalHeaderItem(5, new QTableWidgetItem("Url"));
}

void MainWindow::refreshTaskTable()
{
    ui->task_tableWidget->clear();
    QVector<SubjectModel*> subjectModel = sqlHepler->getSubjectModels();
    ui->task_tableWidget->setRowCount(subjectModel.size());
    for (int j = 0; j < ui->task_tableWidget->colorCount(); j++)
        for(int i = 0; i< ui->task_tableWidget->rowCount(); i++)
            ui->task_tableWidget->setItem(i, j, new QTableWidgetItem());

    for(int i = 0; i < ui->task_tableWidget->rowCount(); i++)
        for(int j = 0; j < ui->task_tableWidget->columnCount(); j++)
        {
            QTableWidgetItem *item = new QTableWidgetItem(); // выделяем память под ячейку
            switch (j) {
            case 0:
                item->setText(QString(subjectModel[i]->id));
                break;
            case 1:
                item->setText(QString(subjectModel[i]->name));
                break;
            case 2:
                item->setText(QString(subjectModel[i]->nameTask));
                break;
            case 3:
                item->setText(QString(subjectModel[i]->descriptionTask));
                break;
            case 4:
                item->setText(QString(subjectModel[i]->literatureTask));
                break;
            }

            ui->task_tableWidget->setItem(i, j, item); // вставляем ячейку
        }

    ui->task_tableWidget->setHorizontalHeaderItem(0, new QTableWidgetItem("Id"));
    ui->task_tableWidget->setHorizontalHeaderItem(1, new QTableWidgetItem("Name subject"));
    ui->task_tableWidget->setHorizontalHeaderItem(2, new QTableWidgetItem("Name task"));
    ui->task_tableWidget->setHorizontalHeaderItem(3, new QTableWidgetItem("Description task"));
    ui->task_tableWidget->setHorizontalHeaderItem(4, new QTableWidgetItem("Literature task"));
}

void MainWindow::on_delete_pushButton_clicked()
{
    if (selectedUserId != -1 ) {
        int id = selectedUserId;
        QString s = QString::number(id);
        if (sqlHepler->deleteUser(s)) {
            refreshUserTable();
        }
    }
}

void MainWindow::on_clearTimetable_pushButton_clicked()
{
    selectedTimetableId = -1;
    clearTimetableSelected();
}

void MainWindow::on_deleteTimetable_pushButton_clicked()
{
    if (selectedTimetableId != -1 ) {
        int id = selectedTimetableId;
        QString s = QString::number(id);
        if (sqlHepler->deleteTimetableSubject(s)) {
            refreshTimetableTable();
        }
    }
}

void MainWindow::on_timatable_tableWidget_clicked(const QModelIndex &index)
{
    clearTimetableSelected();

    int modelIndex = ui->timatable_tableWidget->selectionModel()->currentIndex().row();
    selectedTimetableId = sqlHepler->timetableModels[modelIndex]->id.toInt();

    ui ->nameSub_lineEdit->setText(sqlHepler->timetableModels[modelIndex]->nameSubject);
    ui->time_lineEdit->setText(sqlHepler->timetableModels[modelIndex]->time);
    ui->nameTeacher_lineEdit->setText(sqlHepler->timetableModels[modelIndex]->nameTeacher);
    ui->group_lineEdit->setText(sqlHepler->timetableModels[modelIndex]->group);
    ui->classroom_lineEdit->setText(sqlHepler->timetableModels[modelIndex]->classRoom);

}

void MainWindow::on_addTimetable_pushButton_clicked()
{
    TimetableModel *timetableModel = new TimetableModel();
    QString id = QString::number(sqlHepler->timetableModels[sqlHepler->timetableModels.size()-1]->id.toInt() + 1);
    timetableModel->id = id;
    timetableModel->nameSubject = ui->nameSub_lineEdit->text();
    timetableModel->time = ui->time_lineEdit->text();
    timetableModel->nameTeacher = ui->nameTeacher_lineEdit->text();
    timetableModel->classRoom = ui->classroom_lineEdit->text();
    timetableModel->group = ui->group_lineEdit->text();

    if (timetableModel->nameSubject != "" && timetableModel->time != "" && timetableModel->nameTeacher != ""
            && timetableModel->group != "" &&timetableModel->classRoom != "") {
        sqlHepler->insertNewSubjectInTimetable(timetableModel);
        refreshTimetableTable();
    }
    else {
        QMessageBox::information(0,"Error Insert","Fill in all fields before adding");
    }
}

void MainWindow::on_updateTimetable_pushButton_clicked()
{
    TimetableModel *timetableModel = new TimetableModel();
    QString id = QString::number(sqlHepler->timetableModels[selectedTimetableId-1]->id.toInt());
    timetableModel->id = id;
    timetableModel->nameSubject = ui->nameSub_lineEdit->text();
    timetableModel->time = ui->time_lineEdit->text();
    timetableModel->nameTeacher = ui->nameTeacher_lineEdit->text();
    timetableModel->classRoom = ui->classroom_lineEdit->text();
    timetableModel->group = ui->group_lineEdit->text();
    if (timetableModel->nameSubject != "" && timetableModel->time != "" && timetableModel->nameTeacher != ""
            && timetableModel->group != "" &&timetableModel->classRoom != "" && timetableModel->id != "" && selectedTimetableId-1 != -1) {
        sqlHepler->updateTimetable(timetableModel, selectedTimetableId-1);
        refreshTimetableTable();
   }
}

void MainWindow::on_clearBook_pushButton_clicked()
{
    selectedBookId = -1;
    clearBookSelected();
}

void MainWindow::on_addBook_pushButton_clicked()
{
    LiteratureModel *literatureModel = new LiteratureModel();
    QString id = QString::number(sqlHepler->literatureModels[sqlHepler->literatureModels.size()-1]->id.toInt() + 1);
    literatureModel->id = id;
    literatureModel->subject = ui->subject_lineEdit->text();
    literatureModel->course = ui->courseLit_lineEdit->text();
    literatureModel->nameBook = ui->nameBook_lineEdit->text();
    literatureModel->author = ui->author_lineEdit->text();
    literatureModel->url = ui->url_lineEdit->text();

    if (literatureModel->subject != "" && literatureModel->course != "" && literatureModel->nameBook != ""
            && literatureModel->author != "" &&literatureModel->url != "") {
        sqlHepler->insertNewBook(literatureModel);
        refreshLiteratureTable();
    }
    else {
        QMessageBox::information(0,"Error Insert","Fill in all fields before adding");
    }
}

void MainWindow::on_updateBook_pushButton_clicked()
{
    LiteratureModel *literatureModel = new LiteratureModel();
    QString id = QString::number(sqlHepler->literatureModels[selectedBookId-1]->id.toInt());
    literatureModel->id = id;
    literatureModel->subject = ui->subject_lineEdit->text();
    literatureModel->course = ui->courseLit_lineEdit->text();
    literatureModel->nameBook = ui->nameBook_lineEdit->text();
    literatureModel->author = ui->author_lineEdit->text();
    literatureModel->url = ui->url_lineEdit->text();
    if (literatureModel->subject != "" && literatureModel->course != "" && literatureModel->nameBook != ""
            && literatureModel->author != "" &&literatureModel->url != "" && literatureModel->id != "" && selectedBookId-1 != -1) {
        sqlHepler->updateBook(literatureModel, selectedBookId-1);
        refreshLiteratureTable();
   }
}

void MainWindow::on_literature_tableWidget_clicked(const QModelIndex &index)
{
    clearBookSelected();

    int modelIndex = ui->literature_tableWidget->selectionModel()->currentIndex().row();
    selectedBookId = sqlHepler->literatureModels[modelIndex]->id.toInt();

    ui ->subject_lineEdit->setText(sqlHepler->literatureModels[modelIndex]->subject);
    ui->courseLit_lineEdit->setText(sqlHepler->literatureModels[modelIndex]->course);
    ui->nameBook_lineEdit->setText(sqlHepler->literatureModels[modelIndex]->nameBook);
    ui->author_lineEdit->setText(sqlHepler->literatureModels[modelIndex]->author);
    ui->url_lineEdit->setText(sqlHepler->literatureModels[modelIndex]->url);
}

void MainWindow::on_deleteBook_pushButton_clicked()
{
    if (selectedBookId != -1 ) {
        int id = selectedBookId;
        QString s = QString::number(id);
        if (sqlHepler->deleteBook(s)) {
            refreshLiteratureTable();
        }
    }
}

void MainWindow::on_addTask_pushButton_clicked()
{
    SubjectModel *subjectModel = new SubjectModel();
    QString id = QString::number(sqlHepler->subjectModels[sqlHepler->subjectModels.size()-1]->id.toInt() + 1);
    subjectModel->id = id;
    subjectModel->name = ui->nameSubjectForTask_lineEdit->text();
    subjectModel->nameTask = ui->nameTask_lineEdit->text();
    subjectModel->descriptionTask = ui->descriptionTask_lineEdit->text();
    subjectModel->literatureTask = ui->literatureTask_lineEdit->text();

    if (subjectModel->name != "" && subjectModel->nameTask != "" && subjectModel->descriptionTask != ""
            && subjectModel->literatureTask != "") {
       sqlHepler->insertNewSubjectTask(subjectModel);
        refreshTaskTable();
    }
    else {
        QMessageBox::information(0,"Error Insert","Fill in all fields before adding");
    }
}

void MainWindow::on_updateTask_pushButton_clicked()
{
    SubjectModel *subjectModel = new SubjectModel();
    QString id = QString::number(sqlHepler->subjectModels[selectedTaskId-1]->id.toInt());
    subjectModel->id = id;
    subjectModel->name = ui->nameSubjectForTask_lineEdit->text();
    subjectModel->nameTask = ui->nameTask_lineEdit->text();
    subjectModel->descriptionTask = ui->descriptionTask_lineEdit->text();
    subjectModel->literatureTask = ui->literatureTask_lineEdit->text();
    if (subjectModel->name != "" && subjectModel->nameTask != "" && subjectModel->descriptionTask != ""
            && subjectModel->literatureTask != "" &&  subjectModel->id != "" && selectedTaskId-1 != -1) {
        sqlHepler->updateTask(subjectModel, selectedTaskId-1);
        refreshTaskTable();
   }
}

void MainWindow::on_deleteTask_pushButton_clicked()
{
    if (selectedTaskId != -1 ) {
        int id = selectedTaskId;
        QString s = QString::number(id);
        if (sqlHepler->deleteTask(s)) {
            refreshTaskTable();
        }
    }
}

void MainWindow::on_clearTask_pushButton_clicked()
{
    selectedTaskId = -1;
    clearTaskSelected();
}

void MainWindow::on_task_tableWidget_clicked(const QModelIndex &index)
{
    clearTaskSelected();

    int modelIndex = ui->task_tableWidget->selectionModel()->currentIndex().row();
    selectedTaskId = sqlHepler->subjectModels[modelIndex]->id.toInt();

    ui ->nameSubjectForTask_lineEdit->setText(sqlHepler->subjectModels[modelIndex]->name);
    ui->nameTask_lineEdit->setText(sqlHepler->subjectModels[modelIndex]->nameTask);
    ui->descriptionTask_lineEdit->setText(sqlHepler->subjectModels[modelIndex]->descriptionTask);
    ui->literatureTask_lineEdit->setText(sqlHepler->subjectModels[modelIndex]->literatureTask);
}
