#include "sqlrequesthelper.h"


SqlRequestHelper* SqlRequestHelper::getInstance() {
    static SqlRequestHelper m_instance;
    return &m_instance;
}

bool SqlRequestHelper::connectionToDB(QString login, QString password)
{
    sqlDatabase = QSqlDatabase::addDatabase("QMYSQL");
    sqlDatabase.setHostName("localhost");
    sqlDatabase.setPort(3306);
//    sqlDatabase.setUserName("root");
//    sqlDatabase.setPassword("123123");
        sqlDatabase.setUserName(login);
        sqlDatabase.setPassword(password);
    sqlDatabase.setDatabaseName("studentservicedb");

    qDebug() << "Start";
    if (!sqlDatabase.open()){
        return false;
    }
    else {
        qDebug() << "Connect";
    }
    return true;
}


QVector<UserModel*> SqlRequestHelper::getUserModels()
{
    userModels.clear();
    userModels = *new QVector<UserModel*>();

    QSqlQueryModel model;

    model.setQuery("SELECT * FROM user");

    for (int i = 0; i < model.rowCount(); ++i) {
        UserModel *userModel = new UserModel();
        userModel->id = model.record(i).value("id").toString();
        userModel->firstName =  model.record(i).value("FirstName").toString();
        userModel->lastName = model.record(i).value("LastName").toString();
        userModel->email = model.record(i).value("Email").toString();
        userModel->course = model.record(i).value("Course").toString();
        userModel->password = model.record(i).value("Password").toString();

        userModels.push_back(userModel);
    }
    return userModels;
}

QVector<TimetableModel*> SqlRequestHelper::getTimetableModels()
{
    timetableModels.clear();
    timetableModels = *new QVector<TimetableModel*>();

    QSqlQueryModel model;

    model.setQuery("SELECT * FROM timetable");

    for (int i = 0; i < model.rowCount(); ++i) {
        TimetableModel *timetableModel = new TimetableModel();
        timetableModel->id = model.record(i).value("Id").toString();
        timetableModel->classRoom = model.record(i).value("Classroom").toString();
        timetableModel->group = model.record(i).value("Group").toString();
        timetableModel->nameSubject = model.record(i).value("NameSubject").toString();
        timetableModel->nameTeacher = model.record(i).value("NameTeacher").toString();
        timetableModel->time = model.record(i).value("Time").toString();
        timetableModels.push_back(timetableModel);
    }

    return timetableModels;
}

QVector<LiteratureModel*> SqlRequestHelper::getLiteratureModels()
{
    literatureModels.clear();
    literatureModels = *new QVector<LiteratureModel*>();

    QSqlQueryModel model;

    model.setQuery("SELECT * FROM literature");

    for (int i = 0; i < model.rowCount(); ++i) {
        LiteratureModel *literatureModel = new LiteratureModel();
        literatureModel->id = model.record(i).value("Id").toString();
        literatureModel->subject = model.record(i).value("Subject").toString();
        literatureModel->course = model.record(i).value("Course").toString();
        literatureModel->nameBook = model.record(i).value("NameBook").toString();
        literatureModel->author = model.record(i).value("Author").toString();
        literatureModel->url = model.record(i).value("Url").toString();

        literatureModels.push_back(literatureModel);
    }

    return literatureModels;
}

QVector<SubjectModel*> SqlRequestHelper::getSubjectModels()
{
    subjectModels.clear();
    subjectModels = *new QVector<SubjectModel*>();

    QSqlQueryModel model;

    model.setQuery("SELECT * FROM studentservicedb.subjects");

    for (int i = 0; i < model.rowCount();++i) {
        SubjectModel *subjectModel = new SubjectModel();
        subjectModel->id = model.record(i).value("Id").toString();
        subjectModel->name = model.record(i).value("Name").toString();
        subjectModel->nameTask = model.record(i).value("NameTask").toString();
        subjectModel->descriptionTask = model.record(i).value("DescriptionTask").toString();
        subjectModel->literatureTask = model.record(i).value("LiteratureTask").toString();

        subjectModels.push_back(subjectModel);
    }

    return subjectModels;
}


bool SqlRequestHelper::insertNewUser(UserModel *userModel) {

    QSqlQuery query;
    query.prepare("INSERT user(Id, FirstName,LastName,Email,Course, Password) "
                  "VALUES (?, ?, ?,?,?,?)");
    query.addBindValue(userModel->id.toInt());
    query.addBindValue(userModel->firstName);
    query.addBindValue(userModel->lastName);
    query.addBindValue(userModel->email);
    query.addBindValue(userModel->course.toInt());
    query.addBindValue(userModel->password);
    if (query.exec()) {
        userModels.append(userModel);
        return true;
    }
    return false;
}

bool SqlRequestHelper::insertNewSubjectInTimetable(TimetableModel * timetableModel)
{

    QSqlQuery query;
    query.prepare("INSERT INTO `studentservicedb`.`timetable`(`NameSubject`,`Time`,`NameTeacher`,`Group`,`ClassRoom`) "
                  "VALUES (?, ?, ?,?,?)");
    query.addBindValue(timetableModel->nameSubject);
    query.addBindValue(timetableModel->time);
    query.addBindValue(timetableModel->nameTeacher);
    query.addBindValue(timetableModel->group);
    query.addBindValue(timetableModel->classRoom);

    if (query.exec()) {
        timetableModels.append(timetableModel);
        return true;
    }
    return false;
}

 bool SqlRequestHelper::insertNewBook(LiteratureModel *literatureModel)
 {
     QSqlQuery query;
     query.prepare("INSERT INTO `studentservicedb`.`literature`(`Subject`,`Course`,`NameBook`,`Author`,`Url`) "
                   "VALUES (?, ?, ?,?,?)");

     query.addBindValue(literatureModel->subject);
     query.addBindValue(literatureModel->course);
     query.addBindValue(literatureModel->nameBook);
     query.addBindValue(literatureModel->author);
     query.addBindValue(literatureModel->url);

     if (query.exec()) {
         literatureModels.append(literatureModel);
         return true;
     }
     return false;
 }

 bool SqlRequestHelper::insertNewSubjectTask(SubjectModel *subjectModel)
 {
     QSqlQuery query;
     query.prepare("INSERT INTO `studentservicedb`.`subjects`(`Name`,`NameTask`,`DescriptionTask`,`LiteratureTask`)"
                   "VALUES (?, ?, ?,?)");

     query.addBindValue(subjectModel->name);
     query.addBindValue(subjectModel->nameTask);
     query.addBindValue(subjectModel->descriptionTask);
     query.addBindValue(subjectModel->literatureTask);

     if (query.exec()) {
         subjectModels.append(subjectModel);
         return true;
     }
     return false;
 }

bool SqlRequestHelper::updateUser(UserModel userModel) {

    QSqlQuery query;

    QString command = "UPDATE user SET FirstName = ";
    command += dm + userModel.firstName +dm +", LastName = "+ dm +userModel.lastName+ dm + ", Email = ";
    command +=  dm + userModel.email + dm + ", Course = " + dm + userModel.course + dm + ", Password = " + dm + userModel.password + dm ;
    command +=  " WHERE Id = " + userModel.id + ";";
    qDebug() << command;

    query.prepare(command);

    query.exec();
    if (query.exec()) {
        userModels.at(userModel.id.toInt())->course = userModel.course;
        userModels.at(userModel.id.toInt())->firstName = userModel.firstName;
        userModels.at(userModel.id.toInt())->lastName = userModel.lastName;
        userModels.at(userModel.id.toInt())->email = userModel.email;
        userModels.at(userModel.id.toInt())->password = userModel.password;
        return true;
    }

    return false;
}

bool SqlRequestHelper::updateTimetable(TimetableModel *timetableModel, int index)
{
    QSqlQuery query;
    QString command = "UPDATE `studentservicedb`.`timetable` SET `NameSubject` = ";
    command +=  dm + timetableModel->nameSubject +dm +", `Time` = "+ dm +timetableModel->time+ dm + ", `NameTeacher` = ";
    command +=  dm + timetableModel->nameTeacher + dm + ",  `Group` = " + dm + timetableModel->group + dm + ", `ClassRoom` = " + dm +timetableModel->classRoom+ dm ;
    command +=  " WHERE `Id` = " + timetableModel->id + ";";
    qDebug() << command;

    query.prepare(command);

    query.exec();
    if (query.exec()) {
        timetableModels.at(index)->id = timetableModel->id;
        timetableModels.at(index)->nameSubject = timetableModel->nameSubject;
        timetableModels.at(index)->time = timetableModel->time;
        timetableModels.at(index)->nameTeacher = timetableModel->nameTeacher;
        timetableModels.at(index)->group = timetableModel->group;
        timetableModels.at(index)->classRoom = timetableModel->classRoom;
        return true;
    }

    return false;
}

bool SqlRequestHelper::updateBook(LiteratureModel *literatureModel, int index)
{
    QSqlQuery query;
    QString command = "UPDATE `studentservicedb`.`literature` SET `Subject` = ";
    command +=  dm + literatureModel->subject +dm +", `Course` = "+ dm +literatureModel->course+ dm + ", `NameBook` = ";
    command +=  dm + literatureModel->nameBook + dm + ",  `Author` = " + dm + literatureModel->author + dm + ", `Url` = " + dm +literatureModel->url+ dm ;
    command +=  " WHERE `Id` = " + literatureModel->id + ";";
    qDebug() << command;

    query.prepare(command);

    query.exec();
    if (query.exec()) {
        literatureModels.at(index)->id = literatureModel->id;
        literatureModels.at(index)->subject = literatureModel->subject;
        literatureModels.at(index)->course = literatureModel->course;
        literatureModels.at(index)->nameBook = literatureModel->nameBook;
        literatureModels.at(index)->author = literatureModel->author;
        literatureModels.at(index)->url = literatureModel->url;
        return true;
    }

    return false;
}

bool SqlRequestHelper::updateTask(SubjectModel *subjectModel, int index)
{
    QSqlQuery query;
    QString command = "UPDATE `studentservicedb`.`subjects` SET `Name` = ";
    command +=  dm + subjectModel->name +dm +", `NameTask` = "+ dm +subjectModel->nameTask+ dm + ", `DescriptionTask` = ";
    command +=  dm + subjectModel->descriptionTask + dm + ",  `LiteratureTask` = " + dm + subjectModel->literatureTask + dm;
    command +=  " WHERE `Id` = " + subjectModel->id + ";";
    qDebug() << command;

    query.prepare(command);

    query.exec();
    if (query.exec()) {
        subjectModels.at(index)->id = subjectModel->id;
        subjectModels.at(index)->name = subjectModel->name;
        subjectModels.at(index)->nameTask = subjectModel->nameTask;
        subjectModels.at(index)->descriptionTask = subjectModel->descriptionTask;
        subjectModels.at(index)->literatureTask = subjectModel->literatureTask;
        return true;
    }

    return false;
}

bool SqlRequestHelper::deleteUser(QString id) {
    QSqlQuery query;
    query.prepare("DELETE FROM user WHERE Id = ?");
    query.addBindValue(id.toInt());
    if (query.exec()) {
        return true;
    }
    return false;
}

 bool SqlRequestHelper::deleteTimetableSubject(QString id)
 {
     QSqlQuery query;
     query.prepare("DELETE FROM timetable WHERE Id = ?");
     query.addBindValue(id.toInt());
     if (query.exec()) {
         return true;
     }
     return false;
}

 bool SqlRequestHelper::deleteBook(QString id)
 {
     QSqlQuery query;
     query.prepare("DELETE FROM literature WHERE Id = ?");
     query.addBindValue(id.toInt());
     if (query.exec()) {
         return true;
     }
     return false;
 }

 bool SqlRequestHelper::deleteTask(QString id)
 {
     QSqlQuery query;
     query.prepare("DELETE FROM `studentservicedb`.`subjects` WHERE Id = ?");
     query.addBindValue(id.toInt());
     if (query.exec()) {
         return true;
     }
     return false;
 }

int SqlRequestHelper::qetLastId() {
    return userModels.at(userModels.size()-1)->id.toInt();
}


