#ifndef USERMODEL_H
#define USERMODEL_H
#include <QString>

class UserModel
{
public:
    UserModel();
    UserModel(QString id, QString firstName,QString lastName,QString email,QString course,QString password );
    QString id;
    QString firstName;
    QString lastName;
    QString email;
    QString course;
    QString password;
};

#endif // USERMODEL_H
