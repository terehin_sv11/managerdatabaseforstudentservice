#include "usermodel.h"

UserModel::UserModel(QString id, QString firstName,QString lastName,QString email,QString course,QString password )
{
    this->id = id;
    this->firstName = firstName;
    this->lastName = lastName;
    this->course = course;
    this->password = password;
}

UserModel::UserModel()
{
    id = "-1";
    firstName = "";
    lastName = "";
    email = "";
    course = "-1";
    password = "";
}
