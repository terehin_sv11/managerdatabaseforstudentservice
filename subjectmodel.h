#ifndef SUBJECTMODEL_H
#define SUBJECTMODEL_H
#include <QString>

class SubjectModel
{
public:
    SubjectModel();
    SubjectModel(QString id,QString name,QString nameTask,QString descriptionTask,QString literatureTask);
    QString id;
    QString name;
    QString nameTask;
    QString descriptionTask;
    QString literatureTask;
};

#endif // SUBJECTMODEL_H
