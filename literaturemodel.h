#ifndef LITERATUREMODEL_H
#define LITERATUREMODEL_H
#include <QString>

class LiteratureModel
{
public:
    LiteratureModel();
    LiteratureModel(QString id, QString Subject,QString Course,QString NameBook,QString Author, QString Url);
    QString id;
    QString subject;
    QString course;
    QString nameBook;
    QString author;
    QString url;
};

#endif // LITERATUREMODEL_H
