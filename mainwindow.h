#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QtSql>
#include <QSqlDatabase>
#include <QMessageBox>
#include <QMainWindow>
#include "QDebug"
#include "sqlrequesthelper.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_connectionPushButton_clicked();

    void on_update_pushButton_clicked();

    void on_user_tableWidget_clicked(const QModelIndex &index);

    void on_clear_pushButton_clicked();

    void on_add_pushButton_clicked();

    void refreshUserTable();

    void refreshTimetableTable();

    void refreshLiteratureTable();

    void refreshTaskTable();

    void on_delete_pushButton_clicked();

    void on_clearTimetable_pushButton_clicked();

    void on_deleteTimetable_pushButton_clicked();

    void on_timatable_tableWidget_clicked(const QModelIndex &index);

    void on_addTimetable_pushButton_clicked();

    void on_updateTimetable_pushButton_clicked();

    void on_clearBook_pushButton_clicked();

    void on_addBook_pushButton_clicked();

    void on_updateBook_pushButton_clicked();

    void on_literature_tableWidget_clicked(const QModelIndex &index);

    void on_deleteBook_pushButton_clicked();

    void on_addTask_pushButton_clicked();

    void on_updateTask_pushButton_clicked();

    void on_deleteTask_pushButton_clicked();

    void on_clearTask_pushButton_clicked();

    void on_task_tableWidget_clicked(const QModelIndex &index);

private:
    void clearUserSelected();
    void clearTimetableSelected();
    void clearBookSelected();
    void clearTaskSelected();

    int selectedUserId;
    int selectedTimetableId;
    int selectedBookId;
    int selectedTaskId;

    bool reconectToDB(QString login, QString password);
    bool connect;
    SqlRequestHelper *sqlHepler = SqlRequestHelper::getInstance();
    bool createConnection();
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
